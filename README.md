
# N26

# Architecture

  - Architecture used is MVI

> _MVI_  stands for  _Model-View-Intent_. MVI is one of the newest architecture patterns for Android, inspired by the unidirectional and cyclical nature of the  _Cycle.js_  framework.


![MVI Flow](https://miro.medium.com/max/3000/1*EZODZVenxL7ZEY6OHd61mw.png)

 - **Model**
    This represents the business logic layer
- **View**
 This represents the view layer (typically Android Activities or Fragments)
- **Intent**
 	This represents the actions that can be fired

## MVI Flow
[Activity/Fragment] -> Intent -> [ViewModel] -> ViewState-> [Activity/Fragment]

## Why MVI?
- Unidirectional data flow, all the Intents are going from the user interactions to the ViewModel and all the ViewState are going from the ViewModel to the Activity.
- Reactiveness, the implementation is based on creating and consuming streams. any changes are propagated to other components to be handled

## Clean Architecture
The ViewModel is using UseCases to access the underlying domain behaviour
The UseCases use Repositories to access the actual business logic components (e.g. networking, cache...)

## Modularization

![App modularization](https://i.ibb.co/N995pW4/Screen-Shot-2019-10-04-at-10-08-29-PM.png)

The App is modularized by feature not by layer.

**App contains:**
- `Core` module only contains code and resources which are shared between feature modules.
- `App` module, which is at the same time the feature component.
- If we are going to introduce a new feature we will need to add a new Android component.

## 3rd Party Libraries

- **RxJava**
	- to create streams of intents and view state, as communication between views and view models.
	- Also for background and network calls.
- **RxBinding**
	- to react to user interface events via the RxJava paradigm.
- **Dagger**
	- for dependency injection.
	- Koin is much simpler than Dagger, but Dagger is more powerful as Koin is service locator not dependency injection framework [reddit discussion](https://www.reddit.com/r/androiddev/comments/8ch4cg/dagger2_vs_koin_for_dependency_injection/).
- **Retrofit** and **OkHttp**
	- For API calls.
	- Turns HTTP API into interfaces.
- **Moshi**
	- For Json parsing.
	- Moshi is much faster than Gson.
	- Moshi has much less methods than Gson.
	- Moshi is smaller in size than Gson.

## Testing
- Unit tests for the view model can be found in `MarketPriceViewModelTest`.
- Instrumentation test for the activity can be found in `MarketPriceActivityTest`

### Libs used in testing
- `Mockito`, `Mockito Kotlin` and `Junit` for unit tests.
- `Espresso` for Instrumentation tests.

