package com.melegy.injection

import dagger.Component
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Component providing application wide singletons.
 * To call this make use of N26App.coreComponent or the
 * Activity.coreComponent extension function.
 */
@Component(modules = [CoreDataModule::class])
@Singleton
interface CoreComponent {

    @Component.Builder
    interface Builder {
        fun build(): CoreComponent
    }

    fun provideRetrofit(): Retrofit

    fun provideOkHttp(): OkHttpClient

}
