package com.melegy.injection

interface BaseComponent<T> {

    fun inject(target: T)
}
