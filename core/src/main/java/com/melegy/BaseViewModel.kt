package com.melegy

import androidx.lifecycle.ViewModel
import com.melegy.mvi.MviIntent
import com.melegy.mvi.MviViewModel
import com.melegy.mvi.MviViewState
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel<Intent : MviIntent, ViewState : MviViewState> :
    MviViewModel<Intent, ViewState>, ViewModel() {

    /**
     * Composite Disposable to add disposables to it and clear onClear
     */
    val compositeDisposable = CompositeDisposable()

    /**
     * clear the [compositeDisposable]
     */
    fun onClear() {
        compositeDisposable.clear()
    }
}