package com.melegy.extensions

import android.view.View

/**
 * change view visibility to [View.VISIBLE]
 */
fun View.makeVisible() {
    visibility = View.VISIBLE
}

/**
 * change view visibility to [View.GONE]
 */
fun View.makeGone() {
    visibility = View.GONE
}
