package com.melegy.extensions

import android.app.Activity
import android.widget.Toast

/**
 * Extension function to show toast message
 */
fun Activity.toast(msg: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, msg, duration).show()
}
