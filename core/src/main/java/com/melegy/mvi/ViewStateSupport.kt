package com.melegy.mvi

/**
 * DSL for handling view state in the render functions in the activities
 */
interface ViewStateConsumerScope<V> {
    fun doOnLoading(block: () -> Unit)
    fun doOnSuccess(block: (V) -> Unit)
    fun doOnError(block: (String) -> Unit)
}

fun <Payload> BaseViewState<Payload>.consume(block: ViewStateConsumerScope<Payload>.() -> Unit) {
    block(object : ViewStateConsumerScope<Payload> {
        override fun doOnLoading(block: () -> Unit) {
            if (status == BaseViewState.Status.LOADING) {
                block()
            }
        }

        override fun doOnSuccess(block: (Payload) -> Unit) {
            if (status == BaseViewState.Status.SUCCESS) {
                block(payload!!)
            }
        }

        override fun doOnError(block: (String) -> Unit) {
            if (status == BaseViewState.Status.FAILURE) {
                block(errorMessage!!)
            }
        }

    })
}