package com.melegy.mvi

import io.reactivex.Observable

/**
 * represents the view layer
 */
interface MviView<Intent : MviIntent, ViewState : MviViewState> {
    /**
     * returns Observable of the intents from the view
     *
     * @param isJustCreated indicator if the view is created because of configuration changes
     * or it's the first time
     * true if it's the first time for this view to be created
     * false if it's destroyed and being created again because of configuration changes
     */
    fun intents(isJustCreated: Boolean): Observable<Intent>

    /**
     * render the view state in this view
     *
     * @param viewState the view state to be rendered
     */
    fun render(viewState: ViewState)
}