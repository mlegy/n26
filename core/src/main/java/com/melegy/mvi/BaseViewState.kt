package com.melegy.mvi

/**
 * Base view state that every view state should extend
 */
abstract class BaseViewState<T>(
    /**
     * view state status
     */
    open val status: Status,
    /**
     * payload of the success state, not null when the state is [Status.SUCCESS]
     */
    open val payload: T? = null,
    /**
     * error message to represent the error state, not null when the state is [Status.FAILURE]
     */
    open val errorMessage: String? = null
) : MviViewState {
    enum class Status {
        /**
         * The first state when the view state is just created.
         */
        IDLE,
        /**
         * Indicator that the some intent is fired, and the view state is about to be changed.
         */
        LOADING,
        /**
         * Success state that will come with data in the [payload] in the view state.
         */
        SUCCESS,
        /**
         * Failure state that will come with error message in the [errorMessage] in the view state.
         */
        FAILURE
    }
}