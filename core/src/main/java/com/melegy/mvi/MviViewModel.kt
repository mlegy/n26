package com.melegy.mvi

import io.reactivex.Observable

interface MviViewModel<Intent : MviIntent, ViewState : MviViewState> {
    /**
     * process the intents fired by the view to create results then publish new view states.
     *
     * @param intents observable of intents fired by the view.
     */
    fun processIntents(intents: Observable<Intent>)

    /**
     * stream of view states created by the emitted intents.
     */
    val states: Observable<ViewState>
}