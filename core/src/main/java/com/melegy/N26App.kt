package com.melegy

import android.app.Activity
import android.app.Application
import android.content.Context
import com.melegy.injection.CoreComponent
import com.melegy.injection.DaggerCoreComponent

class N26App : Application() {

    private val coreComponent: CoreComponent by lazy {
        DaggerCoreComponent.create()
    }

    companion object {
        @JvmStatic
        fun coreComponent(context: Context) =
            (context.applicationContext as N26App).coreComponent
    }
}

fun Activity.coreComponent() = N26App.coreComponent(this)
