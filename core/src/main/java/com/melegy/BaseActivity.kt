package com.melegy

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.jakewharton.rxrelay2.PublishRelay
import com.melegy.mvi.MviIntent
import com.melegy.mvi.MviView
import com.melegy.mvi.MviViewState
import io.reactivex.disposables.CompositeDisposable

abstract class BaseActivity<I : MviIntent, R : MviViewState>
    : AppCompatActivity(), MviView<I, R> {

    private lateinit var viewModel: BaseViewModel<I, R>

    /**
     * Composite Disposable to add disposables to it and clear onDestroy
     */
    private val disposable = CompositeDisposable()

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layoutId = getLayoutId()
        if (layoutId != -1) {
            setContentView(getLayoutId())
        }

        // init view model
        initInjection()

        val intents = PublishRelay.create<I>()
        viewModel = viewModel() as BaseViewModel<I, R>
        disposable.add(viewModel.states.subscribe { render(it) })
        viewModel.processIntents(intents)
        disposable.add(intents(savedInstanceState == null).subscribe { intents.accept(it) })
    }

    /**
     * view model of the activity
     */
    abstract fun viewModel(): BaseViewModel<*, *>

    /**
     * used to setup the dependency injection in the activity.
     */
    abstract fun initInjection()

    /**
     * layout res id for this activity or -1 if child will handle calling `setContentView`
     */
    @LayoutRes
    protected abstract fun getLayoutId(): Int

    override fun onDestroy() {
        disposable.clear()
        viewModel.onClear()
        super.onDestroy()
    }

}

