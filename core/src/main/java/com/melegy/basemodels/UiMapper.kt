package com.melegy.basemodels

/**
 * maps between [ItemModel] to another model [UiModel]
 */
interface UiMapper<E : ItemModel, U : UiModel> {

    /**
     * map fom [ItemModel] to [UiModel]
     */
    fun mapToUiModel(model: E): U

}
