package com.melegy.basemodels

/**
 * marker interface for all models that are returned or sent to the repos layer
 */
interface ItemModel
