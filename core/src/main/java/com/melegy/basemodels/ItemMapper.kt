package com.melegy.basemodels

/**
 * maps between [ItemModel] to another model
 * (FooItem <-> FooRemote) or (FooItem <-> FooCache)
 */
interface ItemMapper<R : RemoteModel, E : ItemModel> {

    /**
     * map from [RemoteModel] to [ItemModel]
     */
    fun mapToItem(model: R): E

}
