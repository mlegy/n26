# For stack traces
-keepattributes SourceFile, LineNumberTable

# Get rid of package names, makes file smaller
-repackageclasses

-keep class kotlin.Metadata { *; }

# keep moshi pojos
-keep class com.melegy.basemodels.RemoteModel { *; }
-keep class com.melegy.n26.model.marketprice.remote.MarketPriceRemote { *; }
-keep class com.melegy.n26.model.marketprice.remote.ValueRemote { *; }
