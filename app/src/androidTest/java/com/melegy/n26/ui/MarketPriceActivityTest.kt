package com.melegy.n26.ui

import android.content.pm.ActivityInfo
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.jakewharton.espresso.OkHttp3IdlingResource
import com.melegy.coreComponent
import com.melegy.n26.R
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class MarketPriceActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MarketPriceActivity::class.java)

    private lateinit var resource: OkHttp3IdlingResource

    @Before
    fun setup() {
        val okHttpClient = mActivityTestRule.activity.coreComponent().provideOkHttp()
        resource = OkHttp3IdlingResource.create("OkHttp", okHttpClient)
        IdlingRegistry.getInstance().register(resource)
    }

    @Test
    fun shouldBeAbleToViewFilters() {

        openMenu()

        onView(withText("1 month")).check(matches(isDisplayed()))
        onView(withText("2 months")).check(matches(isDisplayed()))
        onView(withText("3 months")).check(matches(isDisplayed()))
        onView(withText("1 year")).check(matches(isDisplayed()))
        onView(withText("2 years")).check(matches(isDisplayed()))
        onView(withText("All Time")).check(matches(isDisplayed()))
    }

    @Test
    fun shouldBeAbleToSelectOneMonthFilter() {

        openMenu()

        onView(withText("1 month"))
            .perform(click())

        // chart should be shown and nothing else
        verifyOnlyChartVisible()

    }

    @Test
    fun shouldBeAbleToSelectTwoMonthsFilter() {

        openMenu()

        onView(withText("2 months"))
            .perform(click())

        // chart should be shown and nothing else
        verifyOnlyChartVisible()

    }

    @Test
    fun shouldBeAbleToSelectThreeMonthsFilter() {

        openMenu()

        onView(withText("3 months"))
            .perform(click())

        // chart should be shown and nothing else
        verifyOnlyChartVisible()

    }

    @Test
    fun shouldBeAbleToSelectOneYearFilter() {

        openMenu()

        onView(withText("1 year"))
            .perform(click())

        // chart should be shown and nothing else
        verifyOnlyChartVisible()

    }

    @Test
    fun shouldBeAbleToSelectTwoYearsFilter() {

        openMenu()

        onView(withText("2 years"))
            .perform(click())

        // chart should be shown and nothing else
        verifyOnlyChartVisible()

    }

    @Test
    fun shouldBeAbleToSelectAllTimeFilter() {

        openMenu()

        onView(withText("All Time"))
            .perform(click())

        // chart should be shown and nothing else
        verifyOnlyChartVisible()

    }

    @Test
    fun chartShouldSurviveConfigurationChanges() {
        // change screen orientation
        mActivityTestRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        mActivityTestRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        // chart should be shown and nothing else
        verifyOnlyChartVisible()

    }

    @After
    fun clear() {
        IdlingRegistry.getInstance().unregister(resource)
    }

    private fun verifyOnlyChartVisible() {
        onView(withId(R.id.chart)).check(matches(isDisplayed()))
        onView(withId(R.id.progress_bar)).check(matches(not(isDisplayed())))
        onView(withId(R.id.retry_btn)).check(matches(not(isDisplayed())))
    }

    private fun openMenu() {
        onView(withId(R.id.action_bar))
            .perform(click())

        openContextualActionModeOverflowMenu()
    }
}
