package com.melegy.n26.ui

import android.content.Context
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.melegy.n26.R
import com.github.mikephil.charting.utils.MPPointF
import kotlinx.android.synthetic.main.info_window.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Custom implementation of [MarkerView],
 * shows a info window like the one in the blockchain graph.
 * the info window holds: Date, Price and Currency.
 */
class InfoWindowView(context: Context, private val currency: String) :
    MarkerView(context, R.layout.info_window) {

    /**
     * constructor used by tools if needed
     */
    constructor(context: Context) : this(context, "USD")

    override fun refreshContent(e: Entry, highlight: Highlight) {

        val formatter = SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH)

        val seconds = TimeUnit.SECONDS.toMillis(e.x.toLong())
        val date = formatter.format(seconds)

        date_tv.text = date
        currency_tv.text = currency
        value_tv.text = e.y.toInt().toString()

        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }

    companion object {
        /**
         * date format eg 2019/10/04 02:00
         */
        private const val DATE_FORMAT = "yyyy/MM/dd hh:mm"
    }
}