package com.melegy.n26.ui

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.melegy.BaseActivity
import com.melegy.mvi.consume
import com.melegy.n26.presentation.GetMarketPriceIntent
import com.melegy.n26.presentation.MarketPriceViewModel
import com.melegy.n26.presentation.MarketPriceViewState
import com.melegy.n26.R
import io.reactivex.Observable
import javax.inject.Inject
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.activity_market_price.*
import com.github.mikephil.charting.data.LineData
import com.melegy.extensions.toast
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.SimpleDateFormat
import androidx.core.content.ContextCompat
import com.jakewharton.rxbinding3.view.clicks
import com.jakewharton.rxrelay2.BehaviorRelay
import com.melegy.extensions.makeGone
import com.melegy.extensions.makeVisible
import com.melegy.n26.ui.TimeSpan.*
import java.util.*
import java.util.concurrent.TimeUnit

class MarketPriceActivity : BaseActivity<GetMarketPriceIntent, MarketPriceViewState>() {

    private var currentSelectedTimeSpan = ONE_YEAR

    @Inject
    lateinit var viewModel: MarketPriceViewModel

    private val getMarketPriceIntent = BehaviorRelay.create<GetMarketPriceIntent>()

    override fun viewModel() = viewModel

    override fun initInjection() = inject()

    override fun getLayoutId() = R.layout.activity_market_price

    override fun intents(isJustCreated: Boolean): Observable<GetMarketPriceIntent> {
        val retryIntent =
            retry_btn.clicks().map { GetMarketPriceIntent(timeSpan = currentSelectedTimeSpan) }
        /**
         * if the
         */
        if (isJustCreated) {
            getMarketPriceIntent.accept(GetMarketPriceIntent(timeSpan = currentSelectedTimeSpan))
        }
        return Observable.merge(getMarketPriceIntent, retryIntent)
    }

    override fun render(viewState: MarketPriceViewState) {
        viewState.consume {
            doOnLoading {
                hideRetryButton()
                hideChart()
                showLoading()
            }

            doOnSuccess {
                hideLoading()
                renderGraph(
                    entries = it.marketPrice.values,
                    name = it.marketPrice.name,
                    description = it.marketPrice.description,
                    unit = it.marketPrice.unit,
                    timeSpan = it.timeSpan
                )
            }

            doOnError {
                hideLoading()
                showRetryButton()
                // todo better handling for error messages
                toast(it)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val consumed = when (item.itemId) {
            R.id.one_month -> {
                currentSelectedTimeSpan = ONE_MONTH
                true
            }
            R.id.two_months -> {
                currentSelectedTimeSpan = TWO_MONTHS
                true
            }
            R.id.three_months -> {
                currentSelectedTimeSpan = THREE_MONTHS
                true
            }
            R.id.one_year -> {
                currentSelectedTimeSpan = ONE_YEAR
                true
            }
            R.id.two_years -> {
                currentSelectedTimeSpan = TWO_YEARS
                true
            }
            R.id.all_time -> {
                currentSelectedTimeSpan = ALL_TIME
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
        getMarketPriceIntent.accept(GetMarketPriceIntent(currentSelectedTimeSpan))
        return consumed
    }

    private fun showLoading() {
        progress_bar.makeVisible()
    }

    private fun hideLoading() {
        progress_bar.makeGone()
    }

    private fun showRetryButton() {
        retry_btn.makeVisible()
    }

    private fun hideRetryButton() {
        retry_btn.makeGone()
    }

    private fun hideChart() {
        chart.makeGone()
    }

    /**
     * render the graph with the prices entries, name, description, currency and time span.
     */
    private fun renderGraph(
        entries: List<Entry>,
        name: String,
        description: String,
        unit: String,
        timeSpan: TimeSpan
    ) {

        // render title
        title_tv.text = name

        // X-Axis properties
        val xAxis = chart.xAxis
        xAxis.apply {
            position = XAxis.XAxisPosition.BOTTOM
            labelCount = getLabelCount(timeSpan)

            setDrawGridLines(false)

            /**
             * custom value formatter to convert the timestamp to formatted date on the graph x-axis
             */
            valueFormatter = object : ValueFormatter() {
                private val formatter = SimpleDateFormat(timeSpan.getDateFormat(), Locale.ENGLISH)

                override fun getFormattedValue(value: Float): String {
                    val seconds = TimeUnit.SECONDS.toMillis(value.toLong())
                    return formatter.format(seconds)
                }
            }
        }

        // Data set properties
        val dataSet = LineDataSet(entries, unit)
        dataSet.apply {
            setDrawValues(false)
            color = ContextCompat.getColor(this@MarketPriceActivity, R.color.colorPrimary)
            setDrawCircles(false)
            setDrawHighlightIndicators(false)
        }

        // Chart properties
        chart.apply {
            axisRight.isEnabled = false
            axisLeft.setDrawAxisLine(false)
            // todo magic number that should be configured based on values and screen size.
            axisLeft.labelCount = 12
            setScaleEnabled(false)
            this.description.text = description
            data = LineData(dataSet)
            val infoWindowView = InfoWindowView(this@MarketPriceActivity, unit)
            infoWindowView.chartView = chart
            marker = infoWindowView
            invalidate()
            makeVisible()
        }
    }

    /**
     * helper function to get the corresponding max count for labels on the x-axis based on the
     * screen orientation, to make sure the view is well constructed.
     */
    private fun getLabelCount(timeSpan: TimeSpan): Int {
        return when (timeSpan) {
            ONE_MONTH -> resources.getInteger(R.integer.one_month)
            TWO_MONTHS -> resources.getInteger(R.integer.two_months)
            THREE_MONTHS -> resources.getInteger(R.integer.three_months)
            ONE_YEAR -> resources.getInteger(R.integer.one_year)
            TWO_YEARS -> resources.getInteger(R.integer.two_years)
            ALL_TIME -> resources.getInteger(R.integer.all_time)
        }
    }
}