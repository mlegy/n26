package com.melegy.n26.ui

import com.melegy.n26.injection.DaggerMarketPlaceComponent
import com.melegy.n26.injection.MarketPlaceModule
import com.melegy.coreComponent

/**
 * Extension function responsible for setting up dagger for [MarketPriceActivity].
 */
fun MarketPriceActivity.inject() {
    DaggerMarketPlaceComponent.builder()
        .module(MarketPlaceModule(this))
        .coreComponent(coreComponent())
        .build()
        .inject(this)
}