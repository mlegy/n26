package com.melegy.n26.ui

/**
 * Represents the available time spans for getting market price
 */
enum class TimeSpan {
    /**
     * 30 days
     */
    ONE_MONTH,
    /**
     * 60 days
     */
    TWO_MONTHS,
    /**
     * 90 days
     */
    THREE_MONTHS,
    /**
     * 1 year
     */
    ONE_YEAR,
    /**
     * 2 years
     */
    TWO_YEARS,
    /**
     * All the time
     */
    ALL_TIME;

    /**
     * maps the enum value to the accepted string by the API
     * used when sending this time span to the API
     */
    override fun toString(): String {
        return when (this) {
            ONE_MONTH -> "1months"
            TWO_MONTHS -> "2months"
            THREE_MONTHS -> "3months"
            ONE_YEAR -> "1year"
            TWO_YEARS -> "2years"
            ALL_TIME -> "all"
        }
    }

    /**
     * get the corresponding date format for each time span value
     */
    fun getDateFormat(): String {
        return when (this) {
            ONE_MONTH, TWO_MONTHS, THREE_MONTHS -> "dd'.' MMM"
            ONE_YEAR, TWO_YEARS -> "MMM ''yy"
            ALL_TIME -> "yyyy"
        }
    }
}