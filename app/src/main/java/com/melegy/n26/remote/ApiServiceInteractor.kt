package com.melegy.n26.remote

import com.melegy.n26.model.marketprice.remote.MarketPriceRemote
import com.melegy.injection.scope.FeatureScope
import io.reactivex.Single
import javax.inject.Inject

@FeatureScope
class ApiServiceInteractor @Inject constructor(private val apiService: ApiService) {

    fun getMarketPrice(lang: String, timeSpan: String): Single<MarketPriceRemote> {
        return apiService.getMarketPrice(
            /**
             * hard coded value json for the format
             */
            format = "json",
            lang = lang,
            timeSpan = timeSpan
        )
    }
}