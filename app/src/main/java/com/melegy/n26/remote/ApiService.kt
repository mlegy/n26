package com.melegy.n26.remote

import com.melegy.n26.model.marketprice.remote.MarketPriceRemote
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("charts/market-price")
    fun getMarketPrice(
        @Query("format") format: String,
        @Query("lang") lang: String,
        @Query("timespan") timeSpan: String
    ): Single<MarketPriceRemote>
}