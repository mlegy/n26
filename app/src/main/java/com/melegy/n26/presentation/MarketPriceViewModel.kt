package com.melegy.n26.presentation

import com.jakewharton.rxrelay2.BehaviorRelay
import com.melegy.BaseViewModel
import com.melegy.n26.usecase.GetMarketPrice
import io.reactivex.Observable
import io.reactivex.Scheduler
import java.util.*
import javax.inject.Named

class MarketPriceViewModel(
    @Named("MainScheduler")
    private val mainScheduler: Scheduler,

    @Named("IOScheduler")
    private val ioScheduler: Scheduler,

    private val getMarketPrice: GetMarketPrice
) : BaseViewModel<GetMarketPriceIntent, MarketPriceViewState>() {

    override val states: BehaviorRelay<MarketPriceViewState> = BehaviorRelay.create()

    override fun processIntents(intents: Observable<GetMarketPriceIntent>) {
        val getMarketPriceResult = intents.ofType(GetMarketPriceIntent::class.java)
            .flatMap { intent ->
                getMarketPrice(lang = Locale.getDefault().language, timeSpan = intent.timeSpan)
                    .toObservable()
                    .map { MarketPriceResult.Success(it, intent.timeSpan) as MarketPriceResult }
                    .subscribeOn(ioScheduler)
                    .startWith(MarketPriceResult.Loading)
                    .onErrorReturn { MarketPriceResult.Error(it) }
                    .observeOn(mainScheduler)
            }

        var state = MarketPriceViewState()

        compositeDisposable.add(getMarketPriceResult.subscribe {
            state = when (it) {
                MarketPriceResult.Loading -> state.toLoading()
                is MarketPriceResult.Success -> state.toSuccess(it.marketPrice, it.timeSpan)
                is MarketPriceResult.Error -> state.toFailure(it.throwable.localizedMessage)
            }
            states.accept(state)
        })

    }
}