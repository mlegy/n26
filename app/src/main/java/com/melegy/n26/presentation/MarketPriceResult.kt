package com.melegy.n26.presentation

import com.melegy.n26.model.marketprice.ui.MarketPriceUiModel
import com.melegy.mvi.MviResult
import com.melegy.n26.ui.TimeSpan

/**
 * Result created while getting the market price.
 */
sealed class MarketPriceResult : MviResult {
    /**
     * loading indicator.
     */
    object Loading : MarketPriceResult()

    /**
     * Success result with the market price data along with the selected time span.
     */
    data class Success(
        val marketPrice: MarketPriceUiModel,
        val timeSpan: TimeSpan
    ) : MarketPriceResult()

    /**
     * Failure result with the throwable thrown.
     */
    data class Error(val throwable: Throwable) : MarketPriceResult()
}