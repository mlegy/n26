package com.melegy.n26.presentation

import com.melegy.mvi.MviIntent
import com.melegy.n26.ui.TimeSpan

/**
 * intent fired when the user requests to get the market price
 *
 * @param timeSpan selected time span to be used while getting the data.
 */
data class GetMarketPriceIntent(val timeSpan: TimeSpan) : MviIntent