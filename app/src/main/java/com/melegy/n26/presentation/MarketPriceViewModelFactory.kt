package com.melegy.n26.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.melegy.n26.usecase.GetMarketPrice
import io.reactivex.Scheduler

/**
 * Factory for creating [MarketPriceViewModel] with args.
 */
class MarketPriceViewModelFactory(
    private val mainScheduler: Scheduler,
    private val ioScheduler: Scheduler,
    private val getMarketPrice: GetMarketPrice
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        require(modelClass == MarketPriceViewModel::class.java) { "Unknown ViewModel class" }
        return MarketPriceViewModel(
            mainScheduler,
            ioScheduler,
            getMarketPrice
        ) as T
    }

}