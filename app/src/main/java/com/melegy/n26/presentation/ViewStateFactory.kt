package com.melegy.n26.presentation

import com.melegy.n26.model.marketprice.ui.MarketPriceUiModel
import com.melegy.mvi.BaseViewState
import com.melegy.n26.ui.TimeSpan

/**
 * create success view state
 */
fun MarketPriceViewState.toSuccess(
    marketPrice: MarketPriceUiModel,
    timeSpan: TimeSpan
): MarketPriceViewState {
    return copy(
        status = BaseViewState.Status.SUCCESS,
        payload = Payload(marketPrice = marketPrice, timeSpan = timeSpan)
    )
}

/**
 * create loading view state
 */
fun MarketPriceViewState.toLoading(): MarketPriceViewState {
    return copy(status = BaseViewState.Status.LOADING)
}

/**
 * create failure view state
 */
fun MarketPriceViewState.toFailure(errorMessage: String): MarketPriceViewState {
    return copy(status = BaseViewState.Status.FAILURE, errorMessage = errorMessage)
}