package com.melegy.n26.presentation

import com.melegy.n26.model.marketprice.ui.MarketPriceUiModel
import com.melegy.mvi.BaseViewState
import com.melegy.n26.ui.TimeSpan

data class MarketPriceViewState(
    override val status: Status = Status.IDLE,
    override val payload: Payload? = null,
    override val errorMessage: String? = null
) : BaseViewState<Payload>(status, payload, errorMessage)

/**
 * Payload contains the [MarketPriceUiModel] data along with the selected [TimeSpan].
 */
data class Payload(
    val marketPrice: MarketPriceUiModel,
    val timeSpan: TimeSpan
)