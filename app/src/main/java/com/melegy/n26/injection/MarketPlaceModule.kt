package com.melegy.n26.injection

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.melegy.n26.remote.ApiService
import com.melegy.n26.remote.ApiServiceInteractor
import com.melegy.n26.repo.MarketPriceRepo
import com.melegy.n26.repo.MarketPriceRepoImp
import com.melegy.injection.scope.FeatureScope
import com.melegy.n26.presentation.MarketPriceViewModel
import com.melegy.n26.presentation.MarketPriceViewModelFactory
import com.melegy.n26.usecase.GetMarketPrice
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.create
import javax.inject.Named

@Module
class MarketPlaceModule(private val activity: AppCompatActivity) {

    @Provides
    @Named("IOScheduler")
    @FeatureScope
    fun provideIOScheduler() = Schedulers.io()

    @Provides
    @Named("MainScheduler")
    @FeatureScope
    fun provideMainThreadScheduler() = AndroidSchedulers.mainThread()!!


    @Provides
    @FeatureScope
    fun provideMarketPriceRepo(apiServiceInteractor: ApiServiceInteractor): MarketPriceRepo {
        return MarketPriceRepoImp(apiServiceInteractor)
    }

    @Provides
    @FeatureScope
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create()
    }

    @Provides
    @FeatureScope
    fun provideMarketPriceViewModelFactory(
        @Named("MainScheduler")
        mainScheduler: Scheduler,
        @Named("IOScheduler")
        ioScheduler: Scheduler,
        getMarketPrice: GetMarketPrice
    ): MarketPriceViewModelFactory =
        MarketPriceViewModelFactory(
            mainScheduler,
            ioScheduler,
            getMarketPrice
        )

    @Provides
    @FeatureScope
    fun provideMarketPriceViewModel(factory: MarketPriceViewModelFactory): MarketPriceViewModel {
        return ViewModelProviders.of(activity, factory).get(MarketPriceViewModel::class.java)
    }

}