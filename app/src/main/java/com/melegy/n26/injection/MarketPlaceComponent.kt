package com.melegy.n26.injection

import com.melegy.n26.ui.MarketPriceActivity
import com.melegy.injection.BaseComponent
import com.melegy.injection.CoreComponent
import com.melegy.injection.scope.FeatureScope
import dagger.Component

@Component(modules = [MarketPlaceModule::class], dependencies = [CoreComponent::class])
@FeatureScope
interface MarketPlaceComponent : BaseComponent<MarketPriceActivity> {

    @Component.Builder
    interface Builder {

        fun coreComponent(coreComponent: CoreComponent): Builder

        fun build(): MarketPlaceComponent

        fun module(module: MarketPlaceModule): Builder

    }
}
