package com.melegy.n26.model.marketprice.ui

import com.github.mikephil.charting.data.Entry
import com.melegy.basemodels.UiMapper
import com.melegy.n26.model.marketprice.item.MarketPriceItem
import java.lang.UnsupportedOperationException
import java.text.SimpleDateFormat
import java.util.*

class MarketPriceUiMapper : UiMapper<MarketPriceItem, MarketPriceUiModel> {

    override fun mapToUiModel(model: MarketPriceItem): MarketPriceUiModel {
        return with(model) {
            MarketPriceUiModel(
                name = name,
                description = description,
                unit = unit,
                values = values.map { Entry(it.timeStamp.toFloat(), it.price.toFloat()) })
        }
    }

}