package com.melegy.n26.model.marketprice.ui

import com.github.mikephil.charting.data.Entry
import com.melegy.basemodels.UiModel

data class MarketPriceUiModel(
    /**
     * name of the graph
     */
    val name: String,
    /**
     * description of the data in the graph
     */
    val description: String,
    /**
     * the currency of the price
     */
    val unit: String,
    /**
     * the values will be represented as list of [Entry] which is used to represent the data on the
     * graph
     */
    val values: List<Entry>
) : UiModel