package com.melegy.n26.model.marketprice.remote

import com.melegy.basemodels.RemoteModel
import com.squareup.moshi.Json

data class MarketPriceRemote(
    /**
     * Graph name
     */
    @Json(name = "name")
    val name: String,

    /**
     * Graph description
     */
    @Json(name = "description")
    val description: String,

    /**
     * Currency unit
     */
    @Json(name = "unit")
    val unit: String,

    /**
     * list of prices values over time
     */
    @Json(name = "values")
    val values: List<ValueRemote>
) : RemoteModel

data class ValueRemote(
    /**
     * Unix timestamp
     */
    @Json(name = "x")
    val timeStamp: Long,

    /**
     * price
     */
    @Json(name = "y")
    val price: Double
) : RemoteModel