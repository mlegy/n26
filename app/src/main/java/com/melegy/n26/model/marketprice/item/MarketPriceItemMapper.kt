package com.melegy.n26.model.marketprice.item

import com.melegy.basemodels.ItemMapper
import com.melegy.n26.model.marketprice.remote.MarketPriceRemote
import java.lang.UnsupportedOperationException

class MarketPriceItemMapper : ItemMapper<MarketPriceRemote, MarketPriceItem> {

    override fun mapToItem(model: MarketPriceRemote): MarketPriceItem {
        return with(model) {
            MarketPriceItem(
                name = name,
                description = description,
                unit = unit,
                values = values.map {
                    ValueItem(
                        timeStamp = it.timeStamp,
                        price = it.price
                    )
                })
        }
    }

}