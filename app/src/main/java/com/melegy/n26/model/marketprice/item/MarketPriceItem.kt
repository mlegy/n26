package com.melegy.n26.model.marketprice.item

import com.melegy.basemodels.ItemModel

data class MarketPriceItem(
    val name: String,
    val description: String,
    val unit: String,
    val values: List<ValueItem>
) : ItemModel

data class ValueItem(
    val timeStamp: Long,
    val price: Double
) : ItemModel