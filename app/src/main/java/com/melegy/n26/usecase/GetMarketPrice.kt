package com.melegy.n26.usecase

import com.melegy.n26.model.marketprice.ui.MarketPriceUiMapper
import com.melegy.n26.model.marketprice.ui.MarketPriceUiModel
import com.melegy.n26.repo.MarketPriceRepo
import com.melegy.n26.ui.TimeSpan
import io.reactivex.Single
import javax.inject.Inject

/**
 * use case responsible of getting the market price of the bitcoin.
 */
class GetMarketPrice @Inject constructor(private val marketPriceRepo: MarketPriceRepo) {

    operator fun invoke(lang: String, timeSpan: TimeSpan): Single<MarketPriceUiModel> {
        return marketPriceRepo.getMarketPrice(lang, timeSpan)
            .map { MarketPriceUiMapper().mapToUiModel(it) }
    }
}