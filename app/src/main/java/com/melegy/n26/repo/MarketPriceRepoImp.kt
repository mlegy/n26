package com.melegy.n26.repo

import com.melegy.n26.model.marketprice.item.MarketPriceItem
import com.melegy.n26.model.marketprice.item.MarketPriceItemMapper
import com.melegy.n26.remote.ApiServiceInteractor
import com.melegy.injection.scope.FeatureScope
import com.melegy.n26.ui.TimeSpan
import io.reactivex.Single
import javax.inject.Inject

@FeatureScope
class MarketPriceRepoImp @Inject constructor(private val apiServiceInteractor: ApiServiceInteractor) :
    MarketPriceRepo {

    override fun getMarketPrice(lang: String, timeSpan: TimeSpan): Single<MarketPriceItem> {
        return apiServiceInteractor.getMarketPrice(lang, timeSpan.toString())
            .map { MarketPriceItemMapper().mapToItem(it) }
    }

}