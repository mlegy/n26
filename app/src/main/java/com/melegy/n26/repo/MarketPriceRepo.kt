package com.melegy.n26.repo

import com.melegy.n26.model.marketprice.item.MarketPriceItem
import com.melegy.n26.ui.TimeSpan
import io.reactivex.Single

/**
 * The repo responsible for handling the market price queries
 */
interface MarketPriceRepo {

    /**
     * returns market price
     *
     * @param lang language to be used while getting the data.
     * @param timeSpan time span of the data.
     */
    fun getMarketPrice(lang: String, timeSpan: TimeSpan): Single<MarketPriceItem>

}