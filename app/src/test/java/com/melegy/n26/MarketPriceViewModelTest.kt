package com.melegy.n26

import com.github.mikephil.charting.data.Entry
import com.jakewharton.rxrelay2.PublishRelay
import com.melegy.mvi.BaseViewState
import com.melegy.n26.model.marketprice.ui.MarketPriceUiModel
import com.melegy.n26.presentation.GetMarketPriceIntent
import com.melegy.n26.presentation.MarketPriceViewModel
import com.melegy.n26.ui.TimeSpan
import com.melegy.n26.usecase.GetMarketPrice
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MarketPriceViewModelTest {

    @Mock
    lateinit var getMarketPrice: GetMarketPrice

    lateinit var viewModel: MarketPriceViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = MarketPriceViewModel(
            mainScheduler = Schedulers.trampoline(),
            ioScheduler = Schedulers.trampoline(),
            getMarketPrice = getMarketPrice
        )
    }

    @Test
    fun `get market price data should show loading first`() {
        Mockito.`when`(getMarketPrice(LANG, TimeSpan.ONE_YEAR))
            .thenReturn(Single.just(MARKET_PRICE))

        val intents = PublishRelay.create<GetMarketPriceIntent>()

        viewModel.processIntents(intents)

        val testObserver = viewModel.states.test()

        intents.accept(GetMarketPriceIntent(TimeSpan.ONE_YEAR))

        testObserver.assertValueAt(0) { it.status == BaseViewState.Status.LOADING }
    }

    @Test
    fun `get market price data should show success`() {
        Mockito.`when`(getMarketPrice(LANG, TimeSpan.ONE_YEAR))
            .thenReturn(Single.just(MARKET_PRICE))

        val intents = PublishRelay.create<GetMarketPriceIntent>()

        viewModel.processIntents(intents)

        val testObserver = viewModel.states.test()

        intents.accept(GetMarketPriceIntent(TimeSpan.ONE_YEAR))

        testObserver.assertValueAt(1) { it.status == BaseViewState.Status.SUCCESS }
    }

    @Test
    fun `get market price data should show success with prices values`() {
        Mockito.`when`(getMarketPrice(LANG, TimeSpan.ONE_YEAR))
            .thenReturn(Single.just(MARKET_PRICE))

        val intents = PublishRelay.create<GetMarketPriceIntent>()

        viewModel.processIntents(intents)

        val testObserver = viewModel.states.test()

        intents.accept(GetMarketPriceIntent(TimeSpan.ONE_YEAR))

        testObserver.assertValueAt(1) { it.payload!!.marketPrice == MARKET_PRICE }
    }

    @Test
    fun `get market price data should show success with same time span sent`() {
        Mockito.`when`(getMarketPrice(LANG, TimeSpan.ONE_YEAR))
            .thenReturn(Single.just(MARKET_PRICE))

        val intents = PublishRelay.create<GetMarketPriceIntent>()

        viewModel.processIntents(intents)

        val testObserver = viewModel.states.test()

        intents.accept(GetMarketPriceIntent(TimeSpan.ONE_YEAR))

        testObserver.assertValueAt(1) { it.payload!!.timeSpan == TimeSpan.ONE_YEAR }
    }

    companion object {
        private const val LANG = "en"
        private val MARKET_PRICE = MarketPriceUiModel(
            name = "Market Price (USD)",
            description = "Average USD market price across major bitcoin exchanges.",
            values = listOf(
                Entry(1538524800f, 6470.402500000001f),
                Entry(1538611200f, 6563.628333333333f),
                Entry(1538697600f, 6568.549166666667f),
                Entry(1538784000f, 6581.486666666667f),
                Entry(1538870400f, 6558.537499999999f),
                Entry(1538956800f, 6618.567692307693f)
            ), unit = "USD"
        )
    }
}